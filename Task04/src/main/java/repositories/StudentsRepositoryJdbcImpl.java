package repositories;

import models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    // language-SQL
    public static final String GET_ALL_STUDENTS = "select * from students";

    private final DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static Function<ResultSet, Student> studentMapper  = row ->{

        try{

            Long id = row.getLong("id");
            String fName = row.getString("first_name");
            String lName = row.getString("last_name");
            Integer age = row.getInt("age");

            return new Student(id, fName, lName, age);

        }catch (SQLException e){

            throw new RuntimeException(e);

        }

    };


    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try {

            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(GET_ALL_STUDENTS);

            while (rs.next()){

                students.add(studentMapper.apply(rs));

            }

            return students;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void save(Student student) {

        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO students(id, firstName, lastName, age) VALUES("
                  + "'" + student.getId() + " ',"
                   + "'" + student.getFirstName() + " ',"
                   + "'" + student.getLastName() + " ',"
                   + "'" + student.getAge() + "'" + " )" );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Optional<Student> findByID(Long id) {

       try{

           Connection connection = dataSource.getConnection();
           Statement st = connection.createStatement();
           ResultSet set = st.executeQuery("SELECT * FROM students WHERE id=" + id);

           return Optional.of(studentMapper.apply(set));

       } catch (SQLException e) {
           throw new RuntimeException(e);
       }

    }

}
