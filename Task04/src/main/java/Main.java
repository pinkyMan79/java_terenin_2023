import jdbc.SimpleDataSource;
import repositories.StudentsRepository;
import repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;


public class Main {

    public static void main(String[] args) {

        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream(new File("/home/terenin/Documents/java_terenin_2023/Task04/resources/db.properties")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        DataSource dataSource = new SimpleDataSource(prop.getProperty("db.url"), prop.getProperty("db.username"), prop.getProperty("db.password"));

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        System.out.println(studentsRepository.findAll());

    }

}
