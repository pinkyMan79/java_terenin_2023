package ru.itis.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.object.UpdatableSqlQuery;
import ru.itis.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.function.Function;


public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into student(first_name, last_name, email, password) " +
            "values (?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_USER = "UPDATE student SET " +
            "first_name = ?, " +
            "last_name = ?, " +
            "email = ?, " +
            "password = ? " +
            "WHERE id = ?";

    //language=SQL
    private static final String SQL_DELETE_USER = "DELETE FROM student " +
            "WHERE id = ?";

    //language=SQL
    private static final String SQL_Find_All_By_Age_Greater_Than_Order_By_Id_Desc = "SELECT * FROM student " +
            "WHERE age > ? " +
            "ORDER BY id";

    private static final RowMapper<Student> studentMapper = (row,rowNum) -> {

            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return Student.builder().id(id).firstName(firstName).lastName(lastName).age(age).build();

    };

    private final JdbcTemplate template;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Student> findAll() {

        return template.query(SQL_SELECT_ALL_STUDENTS, studentMapper);

    }

    @Override
    public void save(Student student) {

        HashMap<String, Object> parser = new HashMap<>();

        parser.put("first_name", student.getFirstName());
        parser.put("last_name", student.getLastName());
        parser.put("email", student.getEmail());
        parser.put("password", student.getPassword());
        parser.put("phone_number", "default");

        SqlParameterSource params = new MapSqlParameterSource(parser);

        SimpleJdbcInsert insert = new SimpleJdbcInsert(template);
        Long id = insert.withTableName("student").usingGeneratedKeyColumns().executeAndReturnKey(params).longValue();
        student.setId(id); // rember id

    }

    @Override
    public Optional<Student> findById(Long id) {

        return Optional.ofNullable(template.queryForObject(SQL_SELECT_BY_ID, studentMapper, id));

    }

    @Override
    public void update(Student student) {

        Long id = student.getId();

        SqlUpdate update = new SqlUpdate(Objects.requireNonNull(template.getDataSource()), SQL_UPDATE_USER);
        update.declareParameter(new SqlParameter("first_name", Types.CHAR));
        update.declareParameter(new SqlParameter("last_name", Types.CHAR));
        update.declareParameter(new SqlParameter("email", Types.CHAR));
        update.declareParameter(new SqlParameter("password", Types.CHAR));
        update.declareParameter(new SqlParameter("id", Types.INTEGER));
        update.compile();

        update.update(student.getFirstName(), student.getLastName(), student.getEmail(), student.getPassword(), id);

    }

    @Override
    public void delete(Long id) {

        template.update(SQL_DELETE_USER, id);

    }

    @Override
    public List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge) {

        return template.query(SQL_Find_All_By_Age_Greater_Than_Order_By_Id_Desc, studentMapper, minAge);

    }
}
