insert into taxi_order(order_status) VALUES ('In progress');

insert into car(car_model, car_numbers) VALUES ('Toyota Camry 3.5', 'x666ep');
insert into car(car_model, car_numbers) VALUES ('SHIGHOUL', 'c086cp');

insert into driver(first_name, last_name, email, car_id, order_id, is_busy) VALUES ('Valeriy', 'Zhmyshenko', 'pizza114@mail.ru', 1, 1, true);

insert into client(first_name, last_name, email, money_on_count) values ('Terenin','Danila','terenindanila5@gmail.com',100);
insert into client(first_name, last_name, email, money_on_count) values ('Vitaly','Checkushkin','vit@gmail.com',80);
insert into client(first_name, last_name, email, money_on_count, order_id) values ('Mansur','Ziyatdinov','ZzziMan@mail.ru',180, 1);


