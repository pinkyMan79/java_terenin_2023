drop table if exists client;
drop table if exists taxi_order;
drop table if exists driver;
drop table if exists car;


create  table client(

                        id bigserial primary key ,

                        first_name char(20) not null ,

                        last_name char(20) not null ,

                        email char(30) not null,

                        money_on_count bigint not null default '0',

                        order_id bigint

);

create table taxi_order(

                           id bigserial primary key,

                           order_status char(20) not null

);

create table driver(

                       id bigserial primary key,

                       first_name char(20) not null,

                       last_name char(20) not null,

                       email char(30) not null,

                       car_id bigint not null,

                       order_id bigint,

                       is_busy boolean default FALSE

);

create table car(

                    id bigserial primary key,

                    car_model char(40) not null,

                    car_numbers char(6) not null

);

alter table client add foreign key(order_id) references taxi_order(id);

alter table  driver add foreign key(order_id) references  taxi_order(id);
alter table driver add foreign key(car_id) references car(id);


