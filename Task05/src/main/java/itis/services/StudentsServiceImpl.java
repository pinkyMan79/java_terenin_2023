package itis.services;

import itis.dto.StudentSignUp;
import itis.models.Student;
import itis.repositories.StudentsRepository;

/**
 * 11.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword());

        studentsRepository.save(student);
    }
}
