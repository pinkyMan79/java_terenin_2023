/home/terenin/.jdks/openjdk-17.0.1/bin/java -javaagent:/home/terenin/Documents/idea-IU-221.5591.52/lib/idea_rt.jar=38071:/home/terenin/Documents/idea-IU-221.5591.52/bin -Dfile.encoding=UTF-8 -classpath /home/terenin/Documents/untitled/out/production/untitled Main

--task1

SELECT model, speed, hd FROM PC WHERE price < 500

--task2

SELECT DISTINCT maker FROM Product WHERE type = 'Printer'

--task3

select model, ram, screen from Laptop where price > 1000

--task4

select * from Printer where color = 'y'

--task5

select model, speed, hd from PC where (cd = '12x'or cd = '24x') and price < 600

--task6

select distinct maker, Laptop.speed as speed from Product inner join Laptop on Laptop.model = Product.model and Laptop.hd >= 10

--task7

SELECT DISTINCT PC.model, PC.price
FROM PC INNER JOIN Product ON Product.maker = 'B' AND PC.model = Product.model
UNION
SELECT DISTINCT Laptop.model, Laptop.price
FROM Laptop INNER JOIN Product ON Product.maker = 'B' AND Laptop.model = Product.model
UNION
SELECT DISTINCT Printer.model, Printer.price
FROM Printer INNER JOIN Product ON Product.maker = 'B' AND Printer.model = Product.model

--task8

select maker from product where type = 'pc' except select maker from product where type = 'laptop'

--task9

select DISTINCT Product.maker from Product inner join PC on Product.model = PC.model AND PC.speed >= 450

--task10

select model, price from Printer where price = (select MAX(price) from Printer)

--task11

select round(avg(speed), 0) as speed from PC

--task12

select avg(speed) as speed from Laptop where price > 1000

--task13

select avg(speed) from PC inner join Product on Product.maker = 'A' AND Product.model = PC.model

--task14

select Classes.class , Ships.name, country from Classes inner join Ships on Ships.class = Classes.class And Classes.numGuns >= 10

--task15

select distinct hd from PC group by hd having count (model) >= 2

--task16

select distinct A.model as model, B.model as model, A.speed as speed, A.ram as ram from PC as A, PC as B where A.speed = B.speed AND A.ram = B.ram AND A.model > B.model

--task17

select distinct type, model, speed
FROM Laptop, (select type from Product) as Product(type) where speed < all(select speed from PC) and type = 'Laptop'

--task18

select distinct product.maker, printer.price from Product join Printer printer on printer.model = product.model where printer.price = (select min(price)
from printer where color = 'y') and printer.color = 'y'

--task19

Select p.maker, AVG(l.screen) from product p join laptop l on p.model = l.model group by p.maker

--task20

Select maker, count(model) as Count_Model from product WHERE type = 'pc' group by maker having count(model) >= 3

--task21

select maker, max(PC.price) from Product inner join PC on PC.model = Product.model group by maker

--task22

select speed, AVG(price) from PC where speed > 600 group by speed

--task23

select maker from Product inner join PC on PC.model = Product.model where PC.speed >= '750' intersect select maker from Product inner join Laptop on Laptop.model = Product.model where Laptop.speed >= '750'

--task24

with models_max_price as(

select model, price from PC
union
select model, price from Laptop
union
select model, price from Printer

)

select model from models_max_price where price = all(select max(price) from models_max_price)

--task25

select distinct maker from Product inner join PC on PC.model = Product.model where PC.ram = (select min(ram) from PC) AND PC.speed = (select max(speed) from PC where ram = (select min(ram) from PC)) and maker in (select maker from Product where type = 'Printer')

--task26

with pc_laptops as(

select model, price from PC
union all
select model, price from Laptop

)

select avg(price) from pc_laptops inner join Product prod on prod.model = pc_laptops.model where prod.maker = 'A'

--task27

select p.maker, avg(pc.hd) as avg_hd from product p join pc on p.model = pc.model where p.maker in (select maker from product where type = 'printer') group by p.maker

--task28

select count(maker) from (select distinct maker from Product group by maker having count(model) = 1) as Product

--task29

select i.point, i.date, inc, out from income_o i left join outcome_o o on i.point = o.point and i.date = o.date
union
select o.point, o.date, inc, out from income_o i right join outcome_o o on i.point = o.point and i.date = o.date

--task30

select point, date, SUM(sum2), SUM(sum1)
from( select point, date, SUM(inc) as sum1, null as sum2 from Income Group by point, date
Union
select point, date, null as sum1, SUM(out) as sum2 from Outcome group by point, date ) as t
group by point, date order by point

--task31

select class, country from Classes where bore >= 16

--task32

Select country, cast(avg((power(bore,3)/2)) as numeric(6,2)) as mw
from (select country, classes.class, bore, name from classes left join ships on classes.class=ships.class
union all
select distinct country, class, bore, ship from classes tp left join outcomes t on tp.class=t.ship
where ship=class and ship not in (select name from ships) ) a
where name!='null' group by country

--task33

select ship from outcomes where result = 'sunk' and battle = 'North Atlantic'

--task34

select distinct name from Ships inner join Classes on Classes.class = Ships.class where Classes.type = 'bb' and displacement > 35000 and launched >= 1922

--task35

select model, type from Product where model not like '%[^0-9]%' or model not like '%[^a-z]%'

--task36

select distinct c.class from classes c join outcomes out on c.class = out.ship
union
select distinct c.class from classes c join ships s on c.class = s.class where s.class = s.name

--task37

select class from(

select  c.class, s.name from Classes c  join Ships s on s.class = c.class
union
select  c.class, o.ship from Classes c join Outcomes o on o.ship = c.class 

)as head group by class having count(head.class) = 1

--task38

select country from Classes where type = 'bb' 
intersect
select country from Classes where type = 'bc'


--task39

select distinct s.ship from(

select * from Outcomes join Battles on battle = name where result = 'damaged') as s where exists(

	select ship from Outcomes join Battles on battle = name where 			 
        s.ship = ship and s.date < date

)

-- Признаться, редактор отвратительный я полчаса прыгал вокруг select тупо --чтобы найти ошибку, которая вообще с селектом не связана, а вот редактор --так не думал(

--task40

select maker, max(type) as type from Product group by maker having count(model) > 1 and count(distinct type) = 1

--Process finished with exit code 0

